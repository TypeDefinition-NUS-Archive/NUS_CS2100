#include <stdio.h>
#include <string.h>
#include <ctype.h>

int hexToDecimal(char[], size_t);
int hexVal(char);

int main(void) {
	char hex[31];
	size_t len;

	printf("Enter up to 30 hexadecimal characters: ");
	fgets(hex, 31, stdin);
	len = strlen(hex);

	/* End-of-Line Check */
	if(hex[len-1] == '\n') {
		len = len - 1;
		hex[len] = '\0';
	}

	printf("You entered: %s\n", hex);
	printf("The value in decimal is: %d\n", hexToDecimal(hex, len));
	printf("First element of array is (Method 1): %c\n", hex[0]);
	printf("First element of array is (Method 2): %c\n", *hex);
	printf("Address of array is (Method 1): %p\n", hex);
	printf("Address of array is (Method 2): %p\n", &hex[0]);

	return 0;
}

int hexVal(char hex) {
	switch(toupper(hex)) {
		case '0': return 0;
		case '1': return 1;
		case '2': return 2;
		case '3': return 3;
		case '4': return 4;
		case '5': return 5;
		case '6': return 6;
		case '7': return 7;
		case '8': return 8;
		case '9': return 9;
		case 'A': return 10;
		case 'B': return 11;
		case 'C': return 12;
		case 'D': return 13;
		case 'E': return 14;
		case 'F': return 15;
	}
	return 0;
}

/*int hexToDecimal(Char hex[], size_t size) {
	int decimal = 0;
	int power = 1;
	for (size_t i = 0; i < size; ++i) {
		int position = size - 1 - i;
		char* ptr = hex[position];
		decimal += hexVal(hex + position) * power;
		power *= 16;
	}
	return decimal;
}*/

int hexToDecimal(char hex[], size_t size) {
	int decimal = 0;
	int power = 1;
	for (char* ptr = hex + (size - 1); ptr >= hex; --ptr)
	{
		decimal += hexVal(*ptr) * power;
		power *= 16;
	}
	return decimal;
}