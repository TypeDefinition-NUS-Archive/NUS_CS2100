# arrayCount.asm
  .data 
arrayA: .word 11, 0, 31, 22, 9, 17, 6, 9   # arrayA has 8 values
count:  .word 999             # dummy value

  .text
input_array: # user input
    la $t0, arrayA # $t0 = address of arrayA[0]
    addi $t1, $t0, 32 # $t1 = address of arrayA[8]

input_array_loop: # user input loop
    beq $t0, $t1, input_x # exit the loop if we've reached the end of the array

    li $v0, 5 #system call code for read_int
    syscall # read the integer
    sw $v0, 0($t0) # store the integer into array[i]

    addi $t0, $t0, 4 # $t0 = arrayA[++i]
    j input_array_loop

input_x:
    li $v0, 5 #system call code for read_int
    syscall # read the integer
    add $t5, $v0, $0 # $t5 = X
    addi $t6, $t5, -1 # $t6 = Mask of X

main:
    la $t0, arrayA # reset $t0 back to address of arrayA[0]
    
    la $t7, count # $t7 = address of count
    lw $t8, 0($t7) # $t8 = loaded value of count (needed for earlier part of the question)
    add $t9, $0, $0 # $t9 = what the actual value of count is

loop:
    beq $t0, $t1, end # exit the loop if we've reached the end of the array
    lw $t2, 0($t0) # $t2 = arrayA[i]
    and $t3, $t6, $t2 # $t3 = Mask & arrayA[i] 
    
    bne $t3, $0, has_remainder # If arrayA[i] is a power of 2, the remainder should be 0.
    addi $t9, $t9, 1 # ++count

has_remainder:
    addi $t0, $t0, 4 # $t0 = arrayA[++i]
    j loop

end:
    sw $t9, 0($t7) # save count back to data memory

    li $v0, 1 # system call code for print_int
    add $a0, $t9, $0 # $a0 = count
    syscall # print the integer

    # code for terminating program
    li $v0, 10
    syscall
