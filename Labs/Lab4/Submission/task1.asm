# messages.asm
  .data 
str: .asciiz "the answer = "
  .text

main: 
    li $v0, 5 #system call code for read_int
    syscall # read the integer
    add $t0, $v0, $0 # copy the integer from $v0 to $t0
    
    li $v0, 4 # system call code for print_string
    la $a0, str # address of the string to print
    syscall # print the string
    
    li $v0, 1 # system call code for print_int
    add $a0, $t0, $0 # copy the integer from $t0 to $a0
    syscall # print the integer
    
    li $v0, 10 # system call code for exit
    syscall # terminate program
