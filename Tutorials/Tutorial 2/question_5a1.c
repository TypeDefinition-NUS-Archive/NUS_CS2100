int s0 = 0b0000000000011111; // s0 = 31
int t0 = s0;
int t1 = 0x1000000000000000; // t1 = 0x8000 0000

while (t0 != 0) {
    int t2 = t0 + 1; // t2 = 0b0000000000100000
    if (t2 != 0) { // This condition will never hit.
        s0 = s0 ^ t1;
    }
    t0 >>= 1; // Loops 5 times.
}

// What happens if we s0 = s0 XOR t1 an odd number of times.
//  0000000000011111
//  1000000000000000
// =1000000000011111

// What happens if we s0 = s0 XOR t1 an even number of times.
//  1000000000011111
//  1000000000000000
// =0000000000011111 (Back to original)